# TT日程管理V2.0开发系列2——日历控件

---

- [TT日程管理V2.0开发系列1](http://www.hizhaohui.cn/archives/550)
- [TT日程管理V2.0开发系列3](http://www.hizhaohui.cn/archives/569)


思来想去，本着分享精神，我把TT日程管理的部分精要代码开源出来。

###[开源项目地址](http://git.oschina.net/cocobaby/ExpandableCalendar)

###[TT日程管理应用下载](http://openbox.mobilem.360.cn/index/d/sid/2423472)

主要处理一些问题，比如calendar的使用，gridview的使用，gridview在listview中的滚动冲突解决。

项目的结构良好，代码注释也比较多。

##一、项目演示：
![项目演示][1]

##二、使用说明:
1.使用起来很方便，可以在xml中声明或者直接在代码中new一个日历对象
```java
CalendarCardView mCalendarCardView = new CalendarCardView(MainActivity.this);
```
2.需要获取选中的日期，添加监听器
```java
mCalendarCardView.setOnDaySelectListener(new OnDaySelectListener() {

            @Override
            public void onDaySelectListener(Calendar date) {
                String s = date.get(Calendar.YEAR) + "-"
                        + date.get(Calendar.MONTH) + "-"
                        + date.get(Calendar.DAY_OF_MONTH);
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        });
```
3.另外一个API是设置当前日期
```java
mCalendarCardView.setCurrentDate(Calendar.getInstance());
```
##鸣谢

[NineOldAndroids](http://nineoldandroids.com/])

##更多

- [我的个人博客](http://www.hizhaohui.cn)

- [我的开源项目](http://git.oschina.net/cocobaby)

- [我的新浪微博](http://weibo.com/1980495343/profile?rightmod=1&wvr=6&mod=personinfo)

##License

Copyright (c) 2014 Kyson

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

  [1]: http://kkwordpress-wordpress.stor.sinaapp.com/uploads/2014/12/showcase.gif
  

package com.tt.expandablecalendar.ui.widget.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 不带滚动属性的gridview <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author kysonX http://www.hizhaohui.cn/
 */
public class GridViewWithoutScroll extends GridView {
    public GridViewWithoutScroll(Context context) {
        super(context);
    }

    public GridViewWithoutScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}

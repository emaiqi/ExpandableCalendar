package com.tt.expandablecalendar;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.tt.expandablecalendar.ui.widget.calendar.CalendarCardView;
import com.tt.expandablecalendar.ui.widget.calendar.CalendarGridViewAdapter.OnDaySelectListener;

/**
 * 
 * 测试页面<功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author Kyson http://www.hizhaohui.cn/
 */
public class MainActivity extends Activity {
    private ListView mListView;
    private CalendarCardView mCalendarCardView;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCalendarCardView = new CalendarCardView(MainActivity.this);
        mListView = (ListView) this.findViewById(R.id.listView1);

        mAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, getDatas("this is item:"));

        mListView.addHeaderView(mCalendarCardView);
        mListView.setAdapter(mAdapter);
        mCalendarCardView.setCurrentDate(Calendar.getInstance());
        mCalendarCardView.setOnDaySelectListener(new OnDaySelectListener() {

            @Override
            public void onDaySelectListener(Calendar date) {
                String s = date.get(Calendar.YEAR) + "-"
                        + date.get(Calendar.MONTH) + "-"
                        + date.get(Calendar.DAY_OF_MONTH);
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String[] getDatas(String title) {
        String[] strings = new String[30];
        for (int i = 0; i < 30; i++) {
            strings[i] = "this is item " + i;
        }
        return strings;
    }
}
